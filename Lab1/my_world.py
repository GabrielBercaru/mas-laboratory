from base import Agent, Action, Perception
from representation import GridRelativeOrientation, GridOrientation
from gridworld import AbstractGridEnvironment, GridAgentData
from enum import Enum

import random

import time


class MyAgent(Agent):
    """
    Your implementation of a reactive cleaner agent.
    """

    def response(self, perceptions):
        """
        TODO: your code here
        :param perceptions: The perceptions of the agent at each step
        :return: The `Action' that your agent takes after perceiving the environment at each step
        """
        return None

    def __str__(self):
        """
        :return: The 1-character representation of your agent name
        """
        return "1"


class MyAction(Action, Enum):
    """
    Actions for the cleaner agent.
    """

    # The robot must move forward
    FORWARD = 0

    # The robot must turn to the left.
    TURN_LEFT = 1

    # The robot must turn to the right.
    TURN_RIGHT = 2

    # The robot must clean the current tile.
    PICK = 3


class MyAgentPerception(Perception):
    """
    The perceptions of an agent.
    """

    def __init__(self, obstacle_orientations, jTile, orientation, evenTile):
        """
        :param obstacle_orientations: Obstacles perceived. They are percieved as a relative orientation at which an
        obstacle is neighbor to the agent. The orientation is relative to the absolute orientation of the agent.

        :param jTile: True if there is trash in the current tile.
        :param orientation: The indication of the agent's compass.
        ;param evenTile: Indicates if the current tile is on an even column
        """

        self.obstacles = obstacle_orientations
        self.is_over_jtile = jTile
        self.absolute_orientation = orientation
        self.evenTile = evenTile


class MyEnvironment(AbstractGridEnvironment):
    """
    Your implementation of the environment in which cleaner agents work.
    """

    def __init__(self):
        """
        Default constructor. This should call the initialize methods offered by the super class.
        """
        rand_seed = 2
        # rand_seed =  datetime.now()

        print("Seed = %i" % rand_seed)

        super(MyEnvironment, self).__init__()
        self.initialize(10, 10, 5, 5, rand_seed=rand_seed)

    def step(self):
        """
        This method should iterate through all agents, provide them with perceptions, and apply the
        action they return.
        """

        for aData in self._agents:
            """
            TODO
            1. create agent perceptions
            2. get agent action based on perception
            3. implement agent action in environment
            """

            # Get the data from the current agent
            currentAgent = aData.linked_agent
            currentPosition = aData.grid_position
            currentOrientation = aData.current_orientation

            # Get the obstacles around the agent
            obstacles = []
            for orientation in GridRelativeOrientation:
                neighbour = currentPosition.get_neighbour_position(currentOrientation, orientation)

                if neighbour in self._get_x_tiles():
                    obstacles.append(orientation)

            # Define the current agent's perception
            perception = MyAgentPerception(
                obstacles,
                currentPosition in self._get_j_tiles(),
                currentOrientation,
                currentPosition.is_x_even()
            )

            # Get the agent's response, defined as an action
            action = currentAgent.response(perception)
            print(action)

            # Update the agent's info based on his action
            if action == MyAction.FORWARD:
                # We just to go one step forward
                nextPosition = currentPosition.get_neighbour_position(currentOrientation, GridRelativeOrientation.FRONT)
                aData.grid_position = nextPosition
            elif action == MyAction.TURN_RIGHT or action == MyAction.TURN_LEFT:
                # We need to rotate the agent
                # Check where he wants to go
                nextPosition = (None, None)
                if action == MyAction.TURN_RIGHT:
                    nextPosition = currentPosition.get_neighbour_position(currentOrientation,
                                                                          GridRelativeOrientation.RIGHT)
                elif action == MyAction.TURN_LEFT:
                    nextPosition = currentPosition.get_neighbour_position(currentOrientation,
                                                                          GridRelativeOrientation.LEFT)

                # Get the relative orientation of the new position
                relativeOrientation = currentPosition.get_relative_orientation(currentOrientation, nextPosition)
                # Update its orientation
                aData.current_orientation = currentOrientation.compute_relative_orientation(relativeOrientation)
            elif action == MyAction.PICK:
                # Pick up the garbage, we can mark this spot as clean
                self.clean_tile(currentPosition)


class DummyAgent(Agent):
    """
    A simple agent that goes around in a circle and picks trash if it finds any.
    """

    def response(self, perceptions):
        print("Dummy agent sees current tile is %s; current orientation is %s; obstacles at: %s"
              % ("dirty" if perceptions.is_over_jtile else "clean",
                 str(perceptions.absolute_orientation),
                 str(perceptions.obstacles)))

        if perceptions.is_over_jtile:
            return MyAction.PICK

        if GridRelativeOrientation.FRONT in perceptions.obstacles:
            return MyAction.TURN_RIGHT

        return MyAction.FORWARD

    def __str__(self):
        return "D"


class RandomAgent(Agent):
    """
    A simple agent that goes random and picks trash if it finds any.
    """

    def response(self, perceptions):
        print("Dummy agent sees current tile is %s; current orientation is %s; obstacles at: %s"
              % ("dirty" if perceptions.is_over_jtile else "clean",
                 str(perceptions.absolute_orientation),
                 str(perceptions.obstacles)))

        if perceptions.is_over_jtile:
            return MyAction.PICK

        # If there's a wall, random rotate in any direction
        if {GridRelativeOrientation.FRONT_LEFT, GridRelativeOrientation.FRONT,
            GridRelativeOrientation.FRONT_RIGHT} - set(perceptions.obstacles) == set():
            return random.choice([MyAction.TURN_LEFT, MyAction.TURN_RIGHT])
        else:
            return random.choice([MyAction.TURN_LEFT, MyAction.TURN_RIGHT, MyAction.FORWARD])

    def __str__(self):
        return "R"


class DeterministicAgent(Agent):
    """
    A more complex agent that goes in snake way through the room and picks trash if it finds any.
    """

    # Define the walls
    FrontWall = {
        GridRelativeOrientation.FRONT_LEFT,
        GridRelativeOrientation.FRONT,
        GridRelativeOrientation.FRONT_RIGHT
    }

    LeftWall = {
        GridRelativeOrientation.BACK_LEFT,
        GridRelativeOrientation.LEFT,
        GridRelativeOrientation.FRONT_LEFT
    }

    RightWall = {
        GridRelativeOrientation.FRONT_RIGHT,
        GridRelativeOrientation.RIGHT,
        GridRelativeOrientation.BACK_RIGHT
    }

    BackWall = {
        GridRelativeOrientation.BACK_LEFT,
        GridRelativeOrientation.BACK,
        GridRelativeOrientation.BACK_RIGHT
    }

    # Check if there's a wall in the Front
    def isFrontWall(self, obstacles):
        return DeterministicAgent.FrontWall - set(obstacles) == set()

    # Check if there's a wall in the Left
    def isLefttWall(self, obstacles):
        return DeterministicAgent.LeftWall - set(obstacles) == set()

    # Check if there's a wall in the Right
    def isRightWall(self, obstacles):
        return DeterministicAgent.RightWall - set(obstacles) == set()

    # Check if there's a wall in the Back
    def isBackWall(self, obstacles):
        return DeterministicAgent.BackWall - set(obstacles) == set()

    # Get the list of the obstacles that are not from the wall
    def getNonWallObstacles(self, obstacles):
        walls = set()

        if self.isFrontWall(obstacles):
            walls |= DeterministicAgent.FrontWall
        if self.isLefttWall(obstacles):
            walls |= DeterministicAgent.LeftWall
        if self.isRightWall(obstacles):
            walls |= DeterministicAgent.RightWall
        if self.isBackWall(obstacles):
            walls |= DeterministicAgent.BackWall

        return list(set(obstacles) - walls)

    def response(self, perceptions: MyAgentPerception):
        print("Dummy agent sees current tile is %s; current orientation is %s; obstacles at: %s"
              % ("dirty" if perceptions.is_over_jtile else "clean",
                 str(perceptions.absolute_orientation),
                 str(perceptions.obstacles)))

        if perceptions.is_over_jtile:
            return MyAction.PICK

        # If we've encountered a wall, turn right or left in order to move to the next column
        # We only care for NORTH and SOUTH walls, as we always go by columns
        if self.isFrontWall(perceptions.obstacles):
            if perceptions.absolute_orientation == GridOrientation.NORTH:
                return MyAction.TURN_RIGHT
            elif perceptions.absolute_orientation == GridOrientation.SOUTH:
                return MyAction.TURN_LEFT

        # We are in the top side. We need to rotate the agent to face SOUTH if it is on an even tile
        if self.isLefttWall(perceptions.obstacles) and perceptions.absolute_orientation == GridOrientation.EAST:
            if perceptions.evenTile:
                return MyAction.TURN_RIGHT
            else:
                return MyAction.FORWARD

        # We are in the bottom side. We need to rotate the agent to face NORTH if it is on an odd tile
        if self.isRightWall(perceptions.obstacles) and perceptions.absolute_orientation == GridOrientation.EAST:
            if not perceptions.evenTile:
                return MyAction.TURN_LEFT
            else:
                return MyAction.FORWARD

        # We've encountered an obstacle, go around by the left side
        # We don't need to consider the wall as an obstacle now
        realObstacles = self.getNonWallObstacles(perceptions.obstacles)

        # Nothing stops us, we can continue our path
        if GridRelativeOrientation.FRONT not in realObstacles and \
                perceptions.absolute_orientation == GridOrientation.NORTH and \
                not perceptions.evenTile:
            return MyAction.FORWARD
        if GridRelativeOrientation.FRONT not in realObstacles and \
                perceptions.absolute_orientation == GridOrientation.SOUTH and \
                perceptions.evenTile:
            return MyAction.FORWARD

        # We've encountered an obstacle. Check if we are on the correct column before trying to go around it
        if GridRelativeOrientation.FRONT in realObstacles:
            if perceptions.absolute_orientation == GridOrientation.NORTH and not perceptions.evenTile or\
                    perceptions.absolute_orientation == GridOrientation.SOUTH and perceptions.evenTile:
                return MyAction.TURN_LEFT
        # We are on the other side of the obstacle, we can continue our path
        if GridRelativeOrientation.RIGHT in realObstacles and \
                not perceptions.evenTile and \
                perceptions.absolute_orientation == GridOrientation.EAST:
            return MyAction.TURN_LEFT
        if GridRelativeOrientation.RIGHT in realObstacles and \
                perceptions.evenTile and \
                perceptions.absolute_orientation == GridOrientation.WEST:
            return MyAction.TURN_LEFT

        # We still need to go around
        if GridRelativeOrientation.RIGHT in realObstacles:
            return MyAction.FORWARD
        elif GridRelativeOrientation.BACK_RIGHT in realObstacles:
            return MyAction.TURN_RIGHT
        elif GridRelativeOrientation.FRONT_RIGHT in realObstacles:
            return MyAction.FORWARD

        # Nothing stops us, we can continue our path
        return MyAction.FORWARD

    def __str__(self):
        return "B"


class Tester(object):
    DELAY = 0.1

    def __init__(self):
        self.env = MyEnvironment()

        agent_data = GridAgentData(DeterministicAgent(), self.env.get_bottom_left(), GridOrientation.NORTH)
        self.env.add_agent(agent_data)

        self.make_steps()

    def make_steps(self):
        while not self.env.goals_completed():
            self.env.step()

            print(self.env)
            # self.env.displayEnv()

            time.sleep(Tester.DELAY)


if __name__ == "__main__":
    tester = Tester()
    tester.make_steps()
